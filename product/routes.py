from app import app
from product.models import Product
from utils import token_required


@app.route('/insertProducts', methods=['POST'])
@token_required
def add_product(current_user):
    return Product().add()


@app.route('/seeAllProducts', methods=['GET'])
def fetch_product():
    return Product().all()


@app.route('/seeAllProducts/<id>', methods=['GET'])
def findByid(id):
    return Product().findByID(id)